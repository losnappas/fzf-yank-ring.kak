# ╭─────────────╥───────────────────────╮
# │ Authors:    ║ File:                 │
# │ Andrey Orst ║ fzf-yank-ring.kak     │
# │ losnappas   ║                       │
# ╞═════════════╩═══════════════════════╡
# │ Module for selecting items in yank  │
# │ ring for fzf.kak plugin             │
# ╞═════════════════════════════════════╡
# │ GitHub.com/andreyorst/fzf.kak       │
# ╰─────────────────────────────────────╯

# Gotta do these out of ModuleLoaded or we miss every yank before fzf is ran.
declare-option -hidden str-list fzf_yank_ring_history

hook -group fzf-yank-ring global NormalKey '[ydc]' %{
    # Initialize option and add new paste.
    set-option global fzf_yank_ring_history %reg(dquote) %opt(fzf_yank_ring_history)
    fzf-yank-ring-self-update- %opt(fzf_yank_ring_history)
}

define-command -hidden fzf-yank-ring-self-update- -params .. %{ evaluate-commands %sh{
    max_ring_size=100
    if test $# -gt $max_ring_size; then
        # Drop oldest.
        shift;
        # Reset.
        printf 'set-option global fzf_yank_ring_history\n'
        length=$#
        index=$((length - max_ring_size + 1))
        # Re-add everything.
        while test $index -le $length; do
          printf 'set-option -add global fzf_yank_ring_history %%arg(%d)\n' $index
          index=$((index + 1))
        done
    fi
}}

hook global ModuleLoaded fzf %§

hook global -once WinCreate .* %{ try %{
    map global fzf -docstring "open yank-ring" 'y' '<esc>: fzf-yank-ring<ret>'
}}

define-command -hidden fzf-yank-ring %{ evaluate-commands %sh{
    yanks=$(mktemp ${TMPDIR:-/tmp}/kak-fzf-yanks.XXXXXX)
    eval "set -- $kak_quoted_opt_fzf_yank_ring_history"
    while [ $# -gt 0 ]; do
        item=$(printf "%s" "$1" | sed "s/^'//;s/'$//;s/''/'/g" | awk 1 ORS='␤')
        printf "%s\n" "$item" >> $yanks
        shift
    done

    message="Swap between items in yank-ring."
    printf "%s\n" "fzf -kak-cmd %{fzf-yank-ring-set-dquote} -items-cmd %{cat $yanks} -preview -preview-cmd %{--preview 'printf \"%s\\\n\" {} | sed \"s/␤/\\\n/g\"'}"
}}

define-command -hidden fzf-yank-ring-set-dquote -params 1 %{
    set-register dquote %sh{ printf "%s\n" "$1" | sed "s/␤/\n/g" }
}

§
